@extends('app')

@section('content')
	<div>
		{{ $page->content }}
		<br/><br/>
		{{ date("d.m.Y", strtotime($page->created_at)) }}
	</div>
@stop